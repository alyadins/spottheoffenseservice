# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140531151323) do

  create_table "images", force: true do |t|
    t.string  "path"
    t.integer "offense_id"
  end

  add_index "images", ["offense_id"], name: "index_images_on_offense_id"

  create_table "locations", force: true do |t|
    t.float "latitude"
    t.float "longitude", limit: 255
  end

  create_table "offense_types", force: true do |t|
    t.string "title"
    t.text   "description"
    t.string "path_to_small_image"
    t.string "path_to_big_image"
  end

  create_table "offenses", force: true do |t|
    t.datetime "time"
    t.string   "car_number"
    t.text     "comment"
    t.integer  "location_id"
    t.integer  "user_id"
    t.integer  "offense_type_id"
  end

  add_index "offenses", ["location_id"], name: "index_offenses_on_location_id"
  add_index "offenses", ["offense_type_id"], name: "index_offenses_on_offense_type_id"
  add_index "offenses", ["user_id"], name: "index_offenses_on_user_id"

  create_table "users", force: true do |t|
    t.string "phone_number"
    t.string "token"
  end

end
