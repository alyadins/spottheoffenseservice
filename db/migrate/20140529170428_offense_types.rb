class OffenseTypes < ActiveRecord::Migration
  def change
    create_table :offense_types do |t|
      t.string :title
      t.text :description
      t.string :path_to_small_image
      t.string :path_to_big_image
    end
  end
end
