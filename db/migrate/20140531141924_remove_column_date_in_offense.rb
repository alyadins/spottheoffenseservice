class RemoveColumnDateInOffense < ActiveRecord::Migration
  def change
    remove_column :offenses, :date
  end
end
