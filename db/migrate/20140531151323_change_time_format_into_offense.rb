class ChangeTimeFormatIntoOffense < ActiveRecord::Migration
  def change
    change_column :offenses, :time, :datetime
  end
end
