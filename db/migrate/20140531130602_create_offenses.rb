class CreateOffenses < ActiveRecord::Migration
  def change
    create_table :offenses do |t|
      t.date :date
      t.time :time
      t.string :car_number
      t.text :comment
      t.belongs_to :location, index: true
      t.belongs_to :user, index: true
      t.belongs_to :offense_type, index: true
    end
  end
end
