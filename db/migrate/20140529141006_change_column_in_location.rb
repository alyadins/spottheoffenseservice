class ChangeColumnInLocation < ActiveRecord::Migration
  def change
    change_table :locations do |t|
      t.change :longitude, :float
    end
  end
end
