class Response

  def initialize(code="200", message="OK")
    @code = code
    @message = message
  end

  def to_json()
    {
        code: @code,
        message: @message
    }.to_json
  end


end