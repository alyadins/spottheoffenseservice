class Image < ActiveRecord::Base
  validates :path, :offense, presence: true
  belongs_to :offense
end
