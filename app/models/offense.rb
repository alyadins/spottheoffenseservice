class Offense < ActiveRecord::Base
  belongs_to :location, dependent: :destroy
  belongs_to :user
  belongs_to :offense_type
  validates :car_number, format: { with: /(A|B|E|K|M|H|O|P|C|T|X|Y)[0-9]{3}(A|B|E|K|M|H|O|P|C|T|X|Y){2}[0-9]{2}/, message: "bad car number format" }
  validates :location, :user, :offense_type, presence: true

  has_many :images, dependent: :destroy
end
