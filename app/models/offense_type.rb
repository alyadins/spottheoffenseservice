class OffenseType < ActiveRecord::Base
  validates :title, :description, :path_to_small_image, :path_to_big_image, presence: true
  validates :title, uniqueness: true
end
