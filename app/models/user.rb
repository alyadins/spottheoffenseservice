class User < ActiveRecord::Base
  validates :phone_number, :token, presence: true, uniqueness: true
  validates :phone_number, format: { with: /((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}/, message: "bad phone format" }
end
