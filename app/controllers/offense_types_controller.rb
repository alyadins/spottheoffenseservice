class OffenseTypesController < ApplicationController
  before_action :set_offense_type, only: [:show,:destroy]

  @@path_to_image = "/offense_type/"
  # GET /offense_types
  # GET /offense_types.json
  def index
    @offense_types = OffenseType.all
  end

  # GET /offense_types/1
  # GET /offense_types/1.json
  def show
  end

  # GET /offense_types/new
  def new
    @offense_type = OffenseType.new
  end

  # GET /offense_types/1/edit
  def edit
  end

  # POST /offense_types
  # POST /offense_types.json
  def create
    @offense_type = OffenseType.new(offense_type_params)

    small_pic = params[:offense_type][:path_to_small_image]
    big_pic = params[:offense_type][:path_to_big_image]

    @offense_type.path_to_small_image =  @@path_to_image + small_pic.original_filename
    @offense_type.path_to_big_image = @@path_to_image + big_pic.original_filename

    FileUtils.copy(small_pic.tempfile, "#{Rails.public_path}" + @offense_type.path_to_small_image)
    FileUtils.copy(big_pic.tempfile, "#{Rails.public_path}" + @offense_type.path_to_big_image)

    respond_to do |format|
      if @offense_type.save
        format.html { redirect_to @offense_type, notice: 'Offense type was successfully created.' }
        format.json { render :show}
      else
        format.html { render :new }
        format.json { render json: @offense_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    FileUtils.remove("#{Rails.public_path}" + @offense_type.path_to_small_image, force:true)
    FileUtils.remove("#{Rails.public_path}" + @offense_type.path_to_big_image, force:true)
    @offense_type.destroy
    respond_to do |format|
      format.html { redirect_to offense_types_url, notice: 'Offense type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offense_type
      @offense_type = OffenseType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offense_type_params
      params.require(:offense_type).permit(:title, :description, :path_to_small_image, :path_to_big_image)
    end
end
