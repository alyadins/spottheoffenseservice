class Api::OffenseController < ApplicationController

  skip_before_filter :verify_authenticity_token
  require 'response'
  require "base64"

  @@car_number_reg_exp = /(A|B|E|K|M|H|O|P|C|T|X|Y)[0-9]{3}(A|B|E|K|M|H|O|P|C|T|X|Y){2}[0-9]{2}/
  @@path_to_offense = "/offenses/"

  def post
    @car_number = params[:car_number]
    @latitude = params[:latitude]
    @longitude = params[:longitude]
    @time = params[:time]
    @comment = params[:comment]
    @token = params[:token]
    @offense_type = params[:offense_type]
    @images_64 = params[:image]


    if @token.blank? || !User.find_by_token(@token)
      render json: Response.new("408", "invalid token").to_json
      return
    end


    unless parse_time
      render json: Response.new("409", "wrong date").to_json
      return
    end

    if @car_number.blank? || !@car_number.match(@@car_number_reg_exp)
      render json: Response.new("410", "wrong car number").to_json
      return
    end

    if @offense_type.blank? || !OffenseType.exists?(id: @offense_type)
      render json: Response.new("411", "wrong offense type").to_json
      return
    end

    if @latitude.blank? && @longitude.blank?
      @latitude = 0.0
      @longitude = 0.0
    end

    @location = Location.new(latitude: @latitude, longitude: @longitude)
    @location.save

    @offense = Offense.new
    @offense.time = @time
    @offense.car_number = @car_number
    @offense.comment = @comment
    @offense.location = @location
    @offense.user = User.find_by_token(@token)
    @offense.offense_type = OffenseType.find(@offense_type)

    @offense.save

    @path_to_image = @@path_to_offense + @offense.id.to_s + "/"
    FileUtils.mkdir_p("#{Rails.public_path}" + @path_to_image)

    @images_64.each_with_index  do |i, index|
      relative_link = @path_to_image + "image" + index.to_s
      write_to_file_64(i, "#{Rails.public_path}" + relative_link)
      Image.new(path: relative_link, offense: @offense).save
    end

  render json: Response.new.to_json
  end

  def parse_time
    if !@time.blank?
      @time = Time.parse(@time)
      true
    else
      false
    end
  end


  def write_to_file_64(string, file_name)
    string.gsub(" ", "+")
    File.open(file_name, 'wb') do|f|
      f.write(Base64.decode64(string))
    end
  end
end
