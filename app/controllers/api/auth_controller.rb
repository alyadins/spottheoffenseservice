class Api::AuthController < ApplicationController
  skip_before_filter :verify_authenticity_token

  require 'response'
  def phone
    if params[:phone].blank?
      render json: Response.new("401", "check phone param").to_json
      return
    end

    phone = params[:phone]

    if User.find_by_phone_number(phone).blank?
      u = User.new
      u.phone_number = phone
      u.token = generate_token
    else
      u = User.find_by_phone_number phone
      u.token = generate_token
    end

    u.save

    unless u.errors.blank?
      render json: Response.new("402", u.errors.messages[:phone_number][0]).to_json
    else
      render json: {
          code: "200",
          message: "OK",
          token: u.token
      }.to_json
    end
  end

  def pin
    if params[:pin].blank? || params[:token].blank?
      render json: Response.new("403", "check params").to_json
      return
    end

    pin = params[:pin]
    token = params[:token]

    u = User.find_by_token token

    if pin == "55555"
      render json:  Response.new().to_json
    else
      render json: Response.new("404","Invalid pin").to_json
    end
  end

  def generate_token
    Digest::SHA1.hexdigest([Time.now, rand].join)
  end
end
