class Api::OffenseTypeController < ApplicationController
  skip_before_filter :verify_authenticity_token

  require 'response'
  def all
    @offense_types = OffenseType.all

    @offenses = Array.new
    @offense_types.each do |ot|
      @offenses << {
          id: ot.id,
          title: ot.title,
          small_image: "http://" +  request.host_with_port + ot.path_to_small_image
      }
    end
    render json: @offenses.to_json
  end

  def get
    if params[:id].blank?
      render json: Response.new("405", "check id param").to_json
      return
    end

    @id = params[:id]

    if OffenseType.exists?(@id)
      @offence_type = OffenseType.find(@id)
    else
      render json: Response.new("406", "not found").to_json
      return
    end

    render json:  {
        id: @offence_type.id,
        description: @offence_type.description,
        big_image: "http://" + request.host_with_port + @offence_type.path_to_big_image
    }.to_json
  end


end
