json.array!(@offenses) do |offense|
  json.extract! offense, :id, :time, :car_number, :comment, :location_id, :user_id, :offense_type_id
  json.url offense_url(offense, format: :json)
end
