json.array!(@images) do |image|
  json.extract! image, :id, :path, :offense_id
  json.url image_url(image, format: :json)
end
