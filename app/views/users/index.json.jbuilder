json.array!(@users) do |user|
  json.extract! user, :id, :phone_number, :token
end
